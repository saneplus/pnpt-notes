> Author: CyberSecurityUP

# Visit Author's Github

- [**Click here to get redirected to -> CyberSecurityUP**](https://github.com/CyberSecurityUP/PNPT-Preparation-Guide)
- **Author's Social Medias**
	- [LinkedIn: Joas Antonio](https://www.linkedin.com/in/joas-antonio-dos-santos)
	- [Twitter: C0d3Cr4zy](https://twitter.com/C0d3Cr4zy)

# PNPT-Preparation-Guide (Unofficial)

- **PNPT Exam Preparation - TCM Security**

## Table of Content:

1. [**TCM Course Resources and Reviews!**](#TCM-Course-Resources)
1. [**OSINT/Information Gathering/Reconnaissance**](#osint)
1. [**Vulnerability Scanning and Exploitation**](#vuln_scan_enum)
1. [**Buffer Overflow**](#buffer-overflows)
1. [**Pivoting - Windows/Linux**](#pivoting)
1. [**Active Directory**](#AD)
1. [**Lateral Movement - Windows/Linux**](#lateral-movement)
1. [**Windows Pentest**](#Windows-Privilege-Escalation)
1. [**Linux PenTest**](#linux-pentest)
1. [**Web Application PenTest**](#web-app-pentesting)
1. [**Exam Report Writer**](#report)
1. [**Labs**](#labs)

-------------------------------------------------------------------------------------
<a name="TCM-Course-Resources"/>

## TCM Course Resources and Reviews!

1. [**TCM Course Resources**](https://github.com/TCM-Course-Resources)
1. **REVIEWS** [Always Check Official PNPT page for Updated reviews](https://certifications.tcm-sec.com/pnpt/)
	- [**Ingo Kleiber's PNPT review**](https://kleiber.me/blog/2021/08/29/TCM-Security-PNPT-Certification-Seven-Days-of-Penetration-Testing/)
	- [**Allan Jay Dumanhug's PNPT review**](https://atom.hackstreetboys.ph/practical-network-penetration-testing-review/)
	- [Infinite Login PNPT review Blog.](https://infinitelogins.com/2021/07/18/practical-network-penetration-tester-pnpt-exam-review/)
	- [Matt Schmidt's PNTP review Blog](https://mattschmidt.net/2021/05/04/tcm-cpeh-exam-certification-review/)
	- [Hack's to Hack Video Review](https://youtu.be/2jhyPg-yzzs)
	- [theinfosecphoneix review](https://theinfosecphoenix.wordpress.com/2021/06/01/my-experience-taking-the-pnpt-practical-network-penetration-tester-certificate/)

-------------------------------------------------------------------------------------
<a name="osint"/>

## OSINT/Information Gathering

1. [**TCM's OSINT Course**](https://academy.tcm-sec.com/p/osint-fundamentals)

1. [**Trace Labs OSINT Foundations Course**](https://academy.osintcombine.com/p/tracelabstraining)
	- [*Udemy: Information-hacking*](https://www.udemy.com/course/information-hacking/)

	- [*Udemy: The-art-of-reconnaissance*](https://www.udemy.com/course/the-art-of-reconnaissance-information-gathering-techniques/)

	- [*Udemy: Information-gathering-phase-1*](https://www.udemy.com/course/information-gathering-phase-1-of-cyber-security/)

1. [**osintframework**](https://osintframework.com/)

1. [Jivoi - **Awesome-OSINT Github**](https://github.com/jivoi/awesome-osint)

1. [Tracelabs - **Awesome-OSINT Github**](https://github.com/tracelabs/awesome-osint)

1. [Lockfale - **OSINT-Framework Github**](https://github.com/lockfale/OSINT-Framework)

1. [Github Topic: **Information Gathering**](https://github.com/topics/information-gathering)

1. [Kyylee.com - **Active-Information Gathering**](https://www.kyylee.com/oscp-notes/active-information-gathering)

1. [s0wr0b1ndef - **Offsec-Cheatsheet: Information Gathering**](https://github.com/s0wr0b1ndef/Offsec-Exam-Cheatsheet/blob/master/Info%20Gathering.md)

1. [Highon.coffee - **penetration-testing-tools-cheat-sheet**](https://highon.coffee/blog/penetration-testing-tools-cheat-sheet/)

1. [Securitymadesimple - **Active-vs-Passive Recon Blog**](https://www.securitymadesimple.org/cybersecurity-blog/active-vs-passive-cyber-reconnaissance-in-information-security)

	- [INFOSEC TRAIN YouTube - Advanced Penetration Testing: **Active Information Gathering**](https://www.youtube.com/watch?v=eIdVtCQSa3s)

	- [dummies.com - **Passive Information Gathering**]https://www.dummies.com/test-prep/passive-information-gathering-for-pentesting/

	- [dummies.com - **Active Information Gathering**]https://www.dummies.com/test-prep/active-information-gathering-for-pentesting/

1. [Strategic Planning Assessment: Overview and Information Gathering Tools](https://literacybasics.ca/strategic-planning/strategic-planning-assesssment/overview-and-information-gathering-tools/)

1. [**Best Information Gathering Tools in Kali Linux?**](https://linuxhint.com/best-information-gathering-tools-in-kali-linux/)

1. [BullsEye0 - **Dorks Eye: Google Hacking Dork Github Tool**](https://github.com/BullsEye0/dorks-eye)
	- [*Dorks Eye Article*](https://hackingpassion.com/dorks-eye-google-hacking-dork-scraping-and-searching-script/)

1. [**exploit-db.com - Google-hacking-database**](https://www.exploit-db.com/google-hacking-database)

1. [Securitytrails.com - **Google Hacking Techniques/Search Operators**](https://securitytrails.com/blog/google-hacking-techniques)

1. [Leonjza - **Awesome-nmap-grep GitHub**](https://github.com/leonjza/awesome-nmap-grep)

1. [Paralax - **awesome-internet-scanning GitHub**](https://github.com/paralax/awesome-internet-scanning)
	- https://nmap.org/

------------------------------------------------------------------------------------
<a name="vuln_scan_enum"/>

## Vulnerability Scanning and Exploitation

1. [enaqx GitHub - **Awesome-Pentest Vulnerability database**](https://github.com/enaqx/awesome-pentest)

1. [Muhammd GitHub - **Awesome-Pentest Vulnerability database**](https://github.com/Muhammd/Awesome-Pentest)

1. [vip2ip GuitHub - **Awesome-Pentester**](https://githubmemory.com/repo/vip2ip/awesome-pentester)

1. [S3cur3Th1sSh1t GitHub - **Pentest-Tools**](https://github.com/S3cur3Th1sSh1t/Pentest-Tools)

1. [We5ter GitHub - **Scanners-Box**](https://github.com/We5ter/Scanners-Box)

1. [skavngr Github - **rapidscan**](https://github.com/skavngr/rapidscan)

1. [**openvas.org**](https://www.openvas.org/)

1. [**zaproxy.org**](https://www.zaproxy.org/)

--------------------------------------------------------------------
<a name="buffer-overflows"/>

## Buffer Overflow

1. [gh0x0st GitHUb- **Buffer_Overflow**](https://github.com/gh0x0st/Buffer_Overflow)

1. [johnjhacking GitHub- **Buffer-Overflow-Guide**](https://github.com/johnjhacking/Buffer-Overflow-Guide)

1. [Tib3rius/Pentest-Cheatsheets GitHub - **buffer-overflows.rst**](https://github.com/Tib3rius/Pentest-Cheatsheets/blob/master/exploits/buffer-overflows.rst)

1. [justinsteven GitHub- **dostackbufferoverflowgood**](https://github.com/justinsteven/dostackbufferoverflowgood)

1. [V1n1v131r4 GitHUb - **OSCP-Buffer-Overflow**](https://github.com/V1n1v131r4/OSCP-Buffer-Overflow)

1. [joshua17sc GitHub - **Buffer-Overflows**](https://github.com/joshua17sc/Buffer-Overflows)

1. [CyberSecurityUP GitGub - **AWESOME-EXPLOIT-DEVELOPMENT**](https://github.com/CyberSecurityUP/AWESOME-EXPLOIT-DEVELOPMENT)

-----------------------------------------------------------------------------
<a name="pivoting"/>

## Pivoting - Windows/Linux

1. [Swisskyrepo GitHub - **Network Pivoting Techniques**](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Network%20Pivoting%20Techniques.md)

1. [RedTeamOperations GitHub - **PivotSuite**](https://github.com/RedTeamOperations/PivotSuite)

1. [0x36 Github - **VPN Pivot**](https://github.com/0x36/VPNPivot)

1. [zxlim GitHub - **pivot-tunnel**](https://github.com/zxlim/pivot-tunnel)

1. [sshuttle GitHub - **sshuttle**](https://github.com/sshuttle/sshuttle)

1. [rsmudge GitHUb - **Layer2-Pivoting-Client**](https://github.com/rsmudge/Layer2-Pivoting-Client)

1. [pha5matis Github - **port_forwarding_and_tunneling**](https://github.com/pha5matis/Pentesting-Guide/blob/master/port_forwarding_and_tunneling.md)

1. [Quantumcore Github - **Maalik:Network Pivoting and Post Exploitation Framework**](https://github.com/quantumcore/maalik)

1. [Socks5 pivoting over Windows SMB pipes Based on rsocktun **Uages is not spacified, Maybe it's underdevelopment**](https://github.com/mis-team/rsockspipe)

1. [740i GitHub - **pentest-notes/pivoting.md**](https://github.com/740i/pentest-notes/blob/master/pivoting.md)

1. [**Chisel** is a fast TCP/UDP tunnel, transported over HTTP, secured via SSH](https://github.com/jpillora/chisel)

----------------------------------------------------------------------------------------
<a name="AD"/>

## Active Directory 

1. [Hausec Blog: **Pentesting Active Directory**](https://hausec.com/2019/03/05/penetration-testing-active-directory-part-i/)

1. **Labs**	
	- [Pentesteracademy - **Activedirectorylab**](https://www.pentesteracademy.com/activedirectorylab)
	- [*Windows Active Directory Penetration Testing - HackTheBox APT*](https://www.youtube.com/watch?v=BjKcBwkSupY)
	- [Tryhackme -**Attacktive Directory**](https://youtu.be/L8fK5-oTSws)
	- [Tryhackme - **Holo**](https://youtu.be/2YeSp0DMkNQ)
	- [Tryhackme - **Throwback - Part:1**](https://youtu.be/mQT38VR4boQ) (Throwback - 1)
	- [Tryhackme - **Throwback - Part:2**](https://youtu.be/ukFC48bzVSM)
	- [Tryhackme - **Wreath Playlist**](https://youtube.com/playlist?list=PLsqUCyw0Jf9sMYXly0uuwfKMu34roGNwk)
	- [Tryhackme - **Razorblack Walkthrough**](https://motasem-notes.net/active-directory-privilege-escalation-through-sebackupprivilege-tryhackme-razor-black/)
	- [Vlunhub - **Vulnos-v2**](https://benisnous.com/vulnos-v2-vulnhub-walkthrough-boot-to-root/)
	- [Motasem Hamdan - **AD & Windows Server Playlist**](https://youtube.com/playlist?list=PLqM63j87R5p6-Ylxslnzx3o_tNY3bo9fS)

1. [Hacktricks - **Active Directory Methodology**](https://book.hacktricks.xyz/windows/active-directory-methodology)

1. [S1ckB0y1337 GitHub : **Active Directory Exploitation Cheat Sheet**](https://github.com/S1ckB0y1337/Active-Directory-Exploitation-Cheat-Sheet)

1. [ balaasif6789 GitHub - **AD-Pentesting**](https://github.com/balaasif6789/AD-Pentesting)

1. **Pentest in Windows server and AD, WPE, Evasion Techniques By: *Joas Antonio***
	- [**Pentest in Windows server and Active Directory**](https://drive.google.com/file/d/1pb_8i_kc68P_RksLPUFEi9TJwAH_wqvI/view?usp=sharing)
	- [**Windows Privilege Escalation**](https://drive.google.com/file/d/1Hjq_Hc8dQEF_ZhNFtGMrl2GELoryboyW/view?usp=sharing)
	- [**OFFENSIVE SECURITY EVASION TECHNIQUES**](https://drive.google.com/file/d/1znezUNtghkcFhwfKMZmeyNrtdbwBXRsz/view?usp=sharing)

-----------------------------------------------------------------------
<a name="lateral-movement"/>

## Lateral Movement - Windows/Linux 

1. [riccardoancarani - **Lateral Movement | Windows and Active Directory**](https://riccardoancarani.github.io/2019-10-04-lateral-movement-megaprimer/)

1. [specterops - **Offensive Lateral Movement**](https://posts.specterops.io/offensive-lateral-movement-1744ae62b14f)

1. [ired - **Lateral Movement**](https://www.ired.team/offensive-security/lateral-movement)

1. [pentestlab - **Lateral Movement – Services**](https://pentestlab.blog/2020/07/21/lateral-movement-services/)

1. [varonis - **Making the Lateral Move**](https://www.varonis.com/blog/penetration-testing-explained-part-iv-making-the-lateral-move/)

1. [logrhythm - **Lateral Movement and How to Detect It**](https://logrhythm.com/blog/what-is-lateral-movement-and-how-to-detect-it/)

1. [MicrosoftDocs GitHub - **Lateral movement playbook**](https://github.com/MicrosoftDocs/ATADocs/blob/master/ATPDocs/playbook-lateral-movement.md)

1. [rmusser01 GitHub - **Lateral Movement**](https://github.com/rmusser01/Infosec_Reference/blob/master/Draft/ATT%26CK-Stuff/ATT%26CK/Lateral%20Movement.md)

1. [redcanary - **Lateral Movement with Secure Shell (SSH)**](https://redcanary.com/blog/lateral-movement-with-secure-shell/)

1. **Linux Post Exploitation**
	- [mrw0r57 - **Linux Lateral Movement And Exfiltration**](https://mrw0r57.github.io/2020-05-31-linux-post-exploitation-10-4/)
	- [ivanitlearning - **Linux Exploitation – Lateral movement**](https://ivanitlearning.wordpress.com/2019/02/10/linux-exploitation-lateral-movement/)

1. [azeria labs - **Lateral Movement Techniques**](https://azeria-labs.com/lateral-movement/)

----------------------------------------------------------------------------
<a name="Windows-Privilege-Escalation"/>

## Windows Pentest.

1. **Windows Privilege Escalation**
	- [swisskyrepo GitHub - **Windows Privilege Escalation**](https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Privilege%20Escalation.md)
	- [0xsp.com - **privilege-escalation-cheatsheet**](https://0xsp.com/offensive/privilege-escalation-cheatsheet)
	- [pentest.tonyng.net - **windows-privilege-escalation-a-cheatsheet**](https://pentest.tonyng.net/windows-privilege-escalation-a-cheatsheet/)
	- [Shushant747 Blog - **Privilege Escalation Windows**](https://sushant747.gitbooks.io/total-oscp-guide/content/privilege_escalation_windows.html)
	- [fuzzysecurity - **Windows Privilege Escalation Fundamentals**](https://www.fuzzysecurity.com/tutorials/16.html)
	- [Hackingdream - **Windows Privilege Escalation Cheatsheet for OSCP**](https://www.hackingdream.net/2020/03/windows-privilege-escalation-cheatsheet-for-oscp.html)
	- [Hacktricks - **Windows Local Privilege Escalation**](https://book.hacktricks.xyz/windows/windows-local-privilege-escalation)
	- [Joshruppe - **basic-windows-enumeration**](https://joshruppe.com/basic-windows-enumeration/)
	- [Noobsec - **Windows Privilege Escalation Cheatsheet**](https://www.noobsec.net/privesc-windows/)
	- [Bytefellow - **Windows Privilege Escalation Cheatshee**](https://www.bytefellow.com/windows-privilege-escalation-cheatsheet-for-oscp/)
	- [Frizb GitHub- Windows-Privilege-Escalation](https://github.com/frizb/Windows-Privilege-Escalation)
	- [netbiosX GitHub - Windows Privilege Escalation](https://github.com/netbiosX/Checklists/blob/master/Windows-Privilege-Escalation.md)
	- [carlospolop GitHub - **WinPE Tool**](https://github.com/carlospolop/winPE)
	- [carlospolop GitHub - **PEASS-ng Tool**](https://github.com/carlospolop/PEASS-ng)

1. [Lolbas - **Living Off The Land Binaries and Scripts**](https://lolbas-project.github.io/)
	- [LOLBAS-Project Github](https://github.com/LOLBAS-Project/LOLBAS/blob/master/CONTRIBUTING.md)

-------------------------------------------------------------------------------------
<a name="linux-pentest"/>

## Linux PenTest

1. [ankh2054 GitHub - **linux-pentest tool**](https://github.com/ankh2054/linux-pentest)

1. [MrPineMan GitHub - **Awesome Reverse shells**](https://github.com/MrPineMan/Awesome-Reverse-Shell)

1. [Lukechilds Github - **Reverse Shell**](https://github.com/lukechilds/reverse-shell)

1. [WangYihang - **Reverse Shell Manager**](https://github.com/WangYihang/Reverse-Shell-Manager)

1. [modauf GitHub - **Girsh (Golang Interactive Reverse Shell)**](https://github.com/nodauf/Girsh)

1. [mzfr GitHub - **rsh tool**](https://github.com/mzfr/rsh)

1. [carlospolop GitHub - **PEASS-ng Tool**](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite)

1. **Basic Linux Privilege Escalation**
	- [g0tmilk Blog - **Basic Linux Privilege Escalation**](https://blog.g0tmi1k.com/2011/08/basic-linux-privilege-escalation/)
	- [johnjhacking Blog - **linux-privilege-escalation-quick-and-dirty**](https://johnjhacking.com/blog/linux-privilege-escalation-quick-and-dirty/)
	- [0xsp.com Blog - **privilege-escalation-cheatsheet**](https://0xsp.com/offensive/privilege-escalation-cheatsheet)
	- [sushant747 Blog - **privilege_escalation-linux**](https://sushant747.gitbooks.io/total-oscp-guide/content/privilege_escalation_-_linux.html)
	- [hackingarticles.in Blog - **privilege-escalation-cheatsheet-vulnhub**](https://www.hackingarticles.in/privilege-escalation-cheatsheet-vulnhub/)
	- [thehackingnomad.com Blog - **PrivEsc - Linux**](https://blog.thehackingnomad.com/cheat-sheet-series/privesc-linux)

1. [**GTFOBins - curated list of Unix binaries**](https://gtfobins.github.io/)

-------------------------------------------------------------------------------------
<a name="web-app-pentesting"/>

## Web Application PenTest

1. [PacktPublishing - **Mastering-Modern-Web-Penetration-Testing**](https://github.com/PacktPublishing/Mastering-Modern-Web-Penetration-Testing)

1. [infoslack GutHub - **awesome-web-hacking**](https://github.com/infoslack/awesome-web-hacking)

1. [qazbnm456 GitHub - **Awesome Web Security**](https://github.com/qazbnm456/awesome-web-security)

1. [wtsxDev GitHub - **List-of-web-application-security**](https://github.com/wtsxDev/List-of-web-application-security/blob/master/README.md)

1. [kaiiyer GitHub - **web-app-pentesting**](https://github.com/kaiiyer/web-app-pentesting)

1. [portswigger.net - **Web Learning materials and labs**](https://portswigger.net/web-security)

1. [CyberSecurityUP GitHub - **eWPTX-Preparation**](https://github.com/CyberSecurityUP/eWPTX-Preparation)

1. [hahwul GitHub - **WebHackersWeapons**](https://github.com/hahwul/WebHackersWeapons)

1. [thehackingsage GitHub- **hacktronian Tool**](https://github.com/thehackingsage/hacktronian)

1. [Mindmeister Xmind Map - **Web Vulnerablility**](https://www.mindmeister.com/pt/1746180947/web-vulnerability-by-joas-antonio)

-------------------------------------------------------------------------------------
<a name="report"/>

## Exam Report Writer

1. [Semi Yulianto Youtube - **Writing An Effective Penetration Testing Report**](https://www.youtube.com/watch?v=OKN5pUgQKIM)

1. [Thecybermentor Youtube - **Writing a Pentest Report**](https://www.youtube.com/watch?v=EOoBAq6z4Zk)
	- [*tcm-sec.com Report 2021*](https://tcm-sec.com/wp-content/uploads/2021/04/TCMS-Demo-Corp-Security-Assessment-Findings-Report.pdf)
	- [*tcm-sec.com Report 2019*](https://github.com/hmaverickadams/TCM-Security-Sample-Pentest-Report)

1. [ITProTV - **Tips for How to Create a Pen (Penetration) Testing Report**](https://www.youtube.com/watch?v=NEz4SfjjwvU)
	- [Blog - *How to Create a Penetration Test Report*](https://blog.itpro.tv/how-to-create-a-penetration-test-report/)
	- [*Download Report Copy*](https://go.itpro.tv/pentest-report)

1. [Cobalt.io - **How to Write an Effective Pentest Report: Vulnerability Reports**](https://cobalt.io/blog/how-to-write-an-effective-pentest-report-vulnerability-reports)

1. [Tutorialspoint - **Penetration Testing - Report Writing**](https://www.tutorialspoint.com/penetration_testing/penetration_testing_report_writing.htm)

1. [Sans.org - **Writing a Penetration Testing Report**](https://www.sans.org/white-papers/33343/)

1. [hebergementwebs.com - **Penetration Testing - Report Writing**](https://www.hebergementwebs.com/penetration-test-tutorial/penetration-testing-report-writing)

1. [**3 tips for writing a quality penetration testing report**](https://searchsecurity.techtarget.com/tip/3-tips-for-writing-a-quality-penetration-testing-report)

--------------------------------------------------------------------------------
<a name="labs"/>

## Laboratory

### https://tryhackme.com/

### https://www.hackthebox.eu/

### https://www.vulnhub.com/

### https://vulnmachines.com/

### https://www.mindmeister.com/pt/1781013629/the-best-labs-and-ctf-red-team-and-pentest

