> Author : r00tkeysec
> File : Introduction to PEH. 

# Table of Content
- [Introduction](#intro)
- [TCM-Course-Resources](#resource)
- [Installing VBox OVA file](#ova)
- [A Pentester's Day to Day](#daytoday)

--------------------------------------------------------------------------------
<a name="intro"/>

## 1. Introduction Topics to be Covered.

- **Network Refresher.**
- **Introduction Linux.**
- **Introduction Python.**
- **External Network Hacking.**
- **Active Directory Exploitation.**
- **Web Application Exploitation.**
- **Wireless Exploitation.**
- **Report Writing.**
--------------------------------------------------------------------------------
<a name="resource"/>

## 2. Resource.

- [PEH Course Resource GITHUB](https://github.com/TCM-Course-Resources/Practical-Ethical-Hacking-Resources)
- [Discord for TCM](https://discord.com/invite/RHZ7UF7)

--------------------------------------------------------------------------------
<a name="ova"/>

## 3. Kali VBox OVA.

- Download from [kali.org](https://www.kali.org/get-kali/#kali-virtual-machines).
- Go to *file -> Import Appliance* and add *.ova* file, finally click on Import & Agree. Go to settings and change accordingly.
- Default User and Pass: (*kali:kali*).
- Change pass of *kali* user. Now, write following commands.
1. `sudo visudo` add user to #User privilege specification & #Allow members of group to execute any command. 
1. `sudo usermod -aG sudo kali` instead of 'kali' you can write any 'user'.
- Now run `sudo apt update -y && apt upgrade -y && apt dist-upgrade -y`.
- You are good to go for **Hacking**.

------------------------------------------------------------------------------
<a name="daytoday"/>

## 4. A Pentester's Day to Day.

- External/Internal Network.
- Web Application.
- Wireless.
- Physical/Social/Phishing.
- SOC assessment ("Purple Teaming").
- REPORT WRITING.
- Debriefs.

### 4.1 Technical Skills Needed.
#### Base:
	- Linux(Preferably Kali/Parrot)
	- Networking (OSI,Protocols, etc.)
	- Scripting skills(Python,Bash,etc)
	- Solid hacking methodology.
	- Tools familiarity (Metasploit, Burp Suit, Nessus, etc.)
#### Preferred:
	- Active Directory.
	- Wireless Attacks.
	- OWASP top 10.
	- Coding skills (Python, Bash, etc.)


------------------------------------------------------------------------------
	

