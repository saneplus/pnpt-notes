#!/bin/python3

#Sockets
import socket

HOST = '127.0.0.1'
PORT = 7777

"""
AF_INET = Socket family version 4, IPv4
SOCK_STREAM = socket type TCP
SOCK_DGRAM = socket type UDP
"""
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST,PORT))

