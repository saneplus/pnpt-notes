#!/bin/python

# Functions
print("Here is an example function: ")

def who_am_i(): #This is a function
	name = "VK"
	age = 22
	print(f"My name is {name} and my age is {age} years old.")
who_am_i()

#adding parameters

def add_hundred(num):
	print(num + 100)
	
add_hundred(100)

#multiple parameters
def add(x,y):
	z = x+y
	zz = x*y
	zzz = x/y
	print(z, zz, zzz)
add(33,33)

# return 
'''return function always is used as \"store it for later\", call it when necessary.'''
def mul(x,y):
	return x * y 
mul(7,7) #calling mul function will not print anything as it does not know what to do with it.
print(mul(7,7)) #Meanwhile if you want an answer then you need to print and call function.

def nl():
	print("\n")
nl()
