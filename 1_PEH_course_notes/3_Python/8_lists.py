#!/bin/python3

def nl():
	print("\n")


# Lists have brackets []

movies = ["sholey", "The Godfather triology", "The Hangover", "Venom"]
series = ["Halt and Catch Fire", "Mr.Robot", "Loki", "GOT", "Two and The Half Man"]
print(movies[0],series[0])
print(movies[0:3]) # 0: for everything starting from, 3: for anything before it.
print(series[1:])
print(movies[:2]) 
print(movies[-1])
print(len(movies), len(series))
movies.append("The Prestige") #append or add something at the end.
print(movies)
movies.pop(1) # Deleting content in list.
print(movies)
nl()
print(series) # Will print whole list, You can also write print(series[:])
print(series[:], movies[:])


