#!/bin/python3

def nl():
	print("\n")

# Conditional Statements 

def drink(money):
	if money >= 2:
		return "You've got yourself a drink"
	else:
		return "No drink for you!"
print(drink(3))
print(drink(1))

nl()

def alcohol(age,money):
	if (age >= 21) and (money >= 5):
		return "You can have drink"
	elif (age >= 21) and (money < 5):
		return "Come back with more money!"
	elif (age < 21) and (money >=5):
		return "You've got money but not age"
	else:
		return "You have no money and age, stay in school kid!"
print(alcohol(22,1))
print(alcohol(20,1))
print(alcohol(60,200000))

