#!/bin/python3

def nl():
	print("\n")
	
# Looping Methods.

# For loops - start to finish of iterate.

randomlists = ["ball", "pen", "cloths", "cutter", "certificate"]

for i in randomlists:
	print(i)

nl()

# While Loops = Execute as long as true.

i = 1
while i < 10:
	print(i)
	i += 1

