#!/bin/python3
def nl():
	print("\n")
	
#Dictionaries : Key/Value pairs

certs = {"Visvesh Kasodariya":"PNPT", "The Cyber Oppressor":["PNPT", "OSCP", "CCNA", "Sec+", "Net+", "AWS"]}
print(certs)
nl()

employees = {"IT":["Bobby","Sahil","Nemish"], "Finance": ["Atharva","Dweejesh","Poojan","Divyesh"],"HR":["Sahil","Sunnny"]}
print(employees)

employees['Legal'] = ["Dweejesh", "Shreya", "Khyati"] #add new key:value pair
print(employees)

# OR

employees.update({"Sales": ["Hero","Jule"]}) #add new key:value pair
print(employees)
nl()
print("This is visvesh's cert: " + certs.get("Visvesh Kasodariya"))
nl()
certs['Visvesh Kasodariya'] = ["PNPT","Net+"] #Update
print(certs)
