#!/bin/bash

# By the way comment can be written after "#" or between (""" HI """). So lets jump right in.
""" Today we are going to learn diffrent strings."""

#Print String.

print("Hello World!!")
print(""" \nThis is multi line string. Look how awesome it is.
I can print multiple lines and no error.
Guys you should check it out, it is mindblowing!! \n """)

# Concatination 

print("Hi, this string is " + "Concatinated!")

